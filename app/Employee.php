<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    protected $fillable = ['id_number', 'name', 'phone_number', 'address'];

    public function institute()
    {
    	return $this->belongsTo('App\Institute');
    }


    public function position()
    {
    	return $this->belongsTo('App\Position');
    }

    public function rank()
    {
        return $this->belongsTo('App\Rank');
    }

    public function academicDegree()
    {
        return $this->belongsTo('App\AcademicDegree');
    }
}


