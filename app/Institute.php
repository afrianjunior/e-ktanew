<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{

	// protected $table = 'institutes';

    protected $fillable = ['name', 'address', 'phone_number'];

    public function employee()
    {
    	return $this->hasMany('App\Employee');
    }

    public function callCenter()
    {
    	return $this->belongsTo('App\CallCenter');
    }
}
