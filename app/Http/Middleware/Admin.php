<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ( Auth::check() && Auth::user()->level == 'admin' )
        {
            return $next($request);
        }

        return redirect(action('PanelController@dashboard'))->with('error-user', 'Maaf anda tidak dapat mengakses menu tersebut, Anda harus login sebagai Admin');

        
    }
}
