<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::paginate(10);

        return view('cp.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $users = User::lists('email', 'id');

        return view('cp.users.create', compact('users'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->Validate($request, [
            'nama'                  =>  'required | min : 4',
            'email'                 =>  'required | email',
            'password'              =>  'required | min : 4',
            'konfirmasi_password'   =>  'required | required_with:password | same:password'
            ]);

        $user = new User;

        $user->name     =  $request->nama;
        $user->email    =  $request->email;
        $user->password =  bcrypt($request->password);
        $user->level    =  $request->level;

        if ($user->save()) {

            return redirect(action('UserController@index'))->with('success-create', 'User berhasil ditambahkan');    

        }

        return redirect(action('UserController@index'))->with('error-create', 'User gagal ditambahkan');    
        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::whereId($id)->firstOrFail();

        return view('cp.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->Validate($request, [
            'nama'                  =>  'required | min : 4',
            'email'                 =>  'required | email',
            'password'              =>  'required | min : 4',
            'konfirmasi_password'   =>  'required | required_with:password | same:password'
        ]);

        $user = User::whereId($id)->firstOrFail();

        $user->name     =  $request->nama;
        $user->email    =  $request->email;
        $user->password =  bcrypt($request->password);
        $user->level    =  (Auth::user()->level == 'user') ? $user->level : $request->level;

        if($user->save()) {

            if(Auth::user()->level == 'user') {

                return redirect(route('cp.dashboard'))->with('success-update', 'Berhasil mengubah data user.');                
            }

            return redirect(route('cp.users.index'))->with('success-update', 'Berhasil mengubah data user');  

        }

        return redirect(action('UserController@index', $user->id))->with('error-update', 'Gagal mengubah data user');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if ($user->delete()) {

            return redirect(action('UserController@index'))->with('success-delete', 'Data user berhasil dihapus');
        }

        return redirect(action('UserController@index'))->with('success-delete', 'Data user gagal dihapus');
    }
}
