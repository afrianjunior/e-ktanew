<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
use App\Institute;
use App\CallCenter;
use DB;
use Input;

class InstituteController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instansi = Institute::paginate(10);

        // dd($instansi);

        return view('cp.instansi.index', compact('instansi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cp.instansi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->Validate($request, [
            'nama'      => 'required | min:3',
            'alamat'    => 'required | min:3',
            'no_telp'   =>  "required | min:3",
        ]);

        $instansi = new Institute;

        $instansi->name         = $request->nama;
        $instansi->address      = $request->alamat;
        $instansi->phone_number = $request->no_telp;

        if ($instansi->save()) {

            return redirect(action('InstituteController@index'))->with('success-create', 'Data instansi berhasil ditambahkan');

        } else {

            return redirect(action('InstituteController@index'))->with('error-create', 'Data instansi gagal ditambahkan');

        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instansi = Institute::whereId($id)->firstOrFail();        

        return view('cp.instansi.edit', compact('instansi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->Validate($request, [
            'nama'      => 'required | min:3',
            'alamat'    => 'required | min:3',
            'no_telp'   =>  "required | min:3",
        ]);

        $instansi = Institute::whereId($id)->firstOrFail();

        $instansi->name         = $request->nama;
        $instansi->address      = $request->alamat;
        $instansi->phone_number = $request->no_telp;

        if ($instansi->save()) {
            return redirect(action('InstituteController@edit', $instansi->id))->with('success-update', 'Data instansi berhasil diubah');

        } else {
            return redirect(action('InstituteController@edit', $instansi->id))->with('error-update', 'Data instansi gagal diubah');
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $instansi = Institute::find($id);

        if ($instansi->delete()) {
            return redirect(action('InstituteController@index'))->with('success-delete', 'Data berhasil dihapus');

        }

        return redirect(action('InstituteController@index'))->with('error-delete', 'Data gagal dihapus');
    }
}
