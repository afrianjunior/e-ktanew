<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
use App\Institute;
use App\Position;

class PanelController extends Controller
{
    
    /**
     * Display a listing of the dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $pegawai    = Employee::all();
        $instansi   = Institute::all();
        $jabatan    = Position::all();

        return view('cp.dashboard', compact('pegawai', 'instansi', 'jabatan'));
    }

}
