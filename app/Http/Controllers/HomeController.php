<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
use App\CallCenter;
use App\Institute;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.home');
    }   

    public function getPegawai($nip)
    {

        $pegawai = Employee::where('id_number', $nip)->get()->first();

        if($pegawai) {
            $instansi_id = $pegawai->institute->id;
            $cc = CallCenter::where('institutes_id', $instansi_id)->get()->first();

            $all =  'Nip : '.$pegawai->id_number.' | Nama : '.$pegawai->name.' | Instansi : '.$pegawai->institute->name.' | Detail informasi : http://www.localhost:8000/front-pegawai/'.$nip;

            return view('front.pegawai-detail', compact('pegawai', 'cc', 'all'));
        }

        return redirect(action('HomeController@index'))->with('error', 'Masukkan Nip Pegawai dengan benar');
    }
}
