<?php

use App\Institute;
use App\CallCenter;
use Illuminate\Database\Seeder;

class CallCenterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CallCenter::truncate();

        $institutes = Institute::all();

        $institutes->each(function ($institute) {
            $callcenter = factory(CallCenter::class)->create();
            $callcenter->institute_id = $institute->id;
            $callcenter->save();
        }); 
    }
}
