<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt('password'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Employee::class, function (Faker\Generator $faker) {
	return [
		'id_number'  		 => $faker->unique()->randomNumber(),
		'name' 				 => $faker->name,
		'phone_number' 		 => $faker->phoneNumber,
		'address' 			 => $faker->streetAddress,
		'academic_degree_id' => $faker->biasedNumberBetween($min = 1, $max = 10),
	];
});

$factory->define(App\AcademicDegree::class, function (Faker\Generator $faker) {
	return [

	];
});

$factory->define(App\Position::class, function (Faker\Generator $faker) {
	return [
        'name' => implode(' ', $faker->words(3)),
	];
});

$factory->define(App\Institute::class, function (Faker\Generator $faker) {
	return [
        'name'         => implode(' ', $faker->words(5)),
        'address'      => $faker->address,
        'phone_number' => $faker->phoneNumber,
	];
});

$factory->define(App\Rank::class, function (Faker\Generator $faker) {
	return [
        'name' => implode(' ', $faker->words(3)),
	];
});

$factory->define(App\CallCenter::class, function (Faker\Generator $faker) {
    return [
        'phone_number' => $faker->phoneNumber,
    ];
});