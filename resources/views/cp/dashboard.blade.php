@extends('cp.master')

@section('content')

	<div class="row">
		<div class="col-md-12">
			<h1 class="ui header">Dashboard
			<div class="sub header">Walcome {{ Auth::user()->level }}!</div>
			</h1>
			<div class="ui divider"></div>
		</div>
	</div>

	@if(Session::has('error-user'))
		<div class="alert alert-danger">
		{{ Session::get('error-user') }}
		</div>
	@endif

	<div class="ui fluid container">
		<h3 class="ui yellow header">Broadcast Message</h3>
		<div class="ui feed">
			<div class="event">
    		<div class="content">
      		<div class="summary">
        		<a href="http://digitalcrafter.net" class="dc link">digitalcrafter</a>
        		<div class="date">
          		make dreams come true
        		</div>
      		</div>
      		<div class="extra text">
        		digitalcrafter merilis Directory App untuk kota dan kabupaten, saat ini telah hadir versi 1.3.2 dan versi development.
      		</div>
        	<div class="ui divider"></div>
    		</div>
  		</div>
			<div class="event">
    		<div class="content">
      		<div class="summary">
        		<a href="http://digitalcrafter.net" class="dc link">digitalcrafter</a>
        		<div class="date">
          		make dreams come true
        		</div>
      		</div>
      		<div class="extra text">
        		Penambahan 12 fitur di aplikasi eKTA telah di rilis bersamaan di 11 kabupaten/kota, untuk panduan silahkan ke link: <a href="http://digitalcrafter.net">digitalcrafter.net/docs</a>
      		</div>
        	<div class="ui divider"></div>
    		</div>
  		</div>
		</div>
		<h3 class="ui yellow header">Static Employee</h3>
		<div class="ui two column grid stackable">
			<div class="column">
				<h4 class="ui grey header">New Employee</h4>
				<div class="ui middle aligned animated list">
					@foreach($pegawai->take(3) as $peg)
					<div class="item">
    				<img class="ui avatar image" src="{{ asset($peg->photo1) }}">
    				<div class="content">
      				<div class="header">{{ $peg->name }}</div>
							<div class="description">{{ $peg->position->name }}</div>
    				</div>
  				</div>
					@endforeach
				</div>
			</div>
			<div class="column">
				<h4 class="ui grey header">All Data</h4>
				<div class="ui divided list">
					<div class="item">
    				<div class="ui orange horizontal circular label">{{ $pegawai->count() }}</div>
    					Employees
  				</div>
					<div class="item">
    				<div class="ui olive horizontal circular label">{{ $instansi->count() }}</div>
    					Institute
  				</div>
					<div class="item">
    				<div class="ui blue horizontal circular label">{{ $jabatan->count() }}</div>
    					Position
  				</div>
				</div>
			</div>
		</div>
		<!-- <div class="col-md-6 stats">
			<a class="ui violet image label">
  			Jumlah Pegawai
  			<div class="detail">{{ $pegawai->count() }}</div>
			</a>
			<div class="ui statistic">
  			<div class="value">
    			{{ $pegawai->count() }}
  			</div>
  			<div class="label">
    			Pegawai
  			</div>
			</div>
		</div> -->
		<!-- <div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					Pegawai Berdasarkan Instansi
				</div>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>Nama</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							@foreach($instansi as $ins)
							<tr>
								<td>{{ $ins->name }}</td>
								<td>{{ $ins->employee->count() }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div> -->
		<!-- <div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					Pegawai Berdasarkan Jabatan
				</div>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>Nama</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							@foreach( $jabatan as $jab )
							<tr>
								<td>{{ $jab->name }}</td>
								<td>{{ $jab->employee->count() }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div> -->
	</div>
@endsection
