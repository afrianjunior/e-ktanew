@extends('cp.master')

@section('content')

<div class="ui grid">
	<div class="sixteen wide column">
		<h1 class="ui header">Mengubah Data Instansi</h1>

		@if(Session::has('error'))
			<div class="alert alert-danger">
				{{ Session::get('error') }}
			</div>
		@endif

	    @if(session('success-update'))
	      	<div class="alert alert-success">
	           	{{ session('success-update') }}
	       	</div>
	    @elseif(session('error-update'))
	       	<div class="alert alert-danger">
	          	{{ session('error-update') }}
	       	</div>
	    @endif

		{!! Form::model($instansi, ['route' => ['cp.instansi.update', $instansi->id], 'method' => 'put', 'files' => true, 'class' => 'ui form']) !!}

			<div class="field {{ ($errors->first('nama')) ? 'has-error' : '' }}">
				<label for="nama">Nama</label>
				{!! Form::text('nama', $instansi->name, ['class' => 'form-control']) !!}
				{!! ($errors->first('nama')) ? $errors->first('nama', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="field {{ ($errors->first('no_telp')) ? 'has-error' : '' }}">
				<label for="no_telp">No. Telepon</label>
				{!! Form::text('no_telp', $instansi->phone_number, ['class' => 'form-control']) !!}
				{!! ($errors->first('no_telp')) ? $errors->first('no_telp', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="field {{ ($errors->first('alamat')) ? 'has-error' : '' }}">
				<label for="alamat">Alamat</label>
				{!! Form::textarea('alamat', $instansi->address, ['rows' => 3, 'class' => 'form-control']) !!}
				{!! ($errors->first('alamat')) ? $errors->first('alamat', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<hr>

			<div class="field">
				<button type="submit" class="ui orange basic button"><i class="save icon"></i>Save</button>
			</div>

		{!! Form::close() !!}
	</div>
</div>

@endsection
