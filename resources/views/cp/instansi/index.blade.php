@extends('cp.master')

@section('content')

	<div class="ui grid">
		<div class="sixteen wide column">
			<h1 class="ui header">Institute<a href="{{ route('cp.instansi.create') }}" class="ui teal basic right floated button"><i class="plus icon"></i>Add Data</a></h1>

	        @if (session('error-delete'))
	          <div class="alert alert-danger">
	            {{ session('error-delete') }}
	          </div>
	        @endif
	        @if (session('success-delete'))
	          <div class="alert alert-success">
	            {{ session('success-delete') }}
	          </div>
	        @endif

	        @if(session('success-create'))
	        	<div class="alert alert-success">
	            	{{ session('success-create') }}
	          	</div>
	        @elseif(session('error-create'))
	        	<div class="alert alert-danger">
	            	{{ session('error-create') }}
	          	</div>
	         @endif

			<table class="ui very basic celled table">
				<thead>
					<tr>
						<th>Nama Instansi</th>
						<th>Alamat</th>
						<th>Nomor Telepon</th>
						<th style="width: 100px">Aksi</th>
					</tr>
				</thead>
				<tbody>

					@foreach($instansi as $ins)

						<tr>
							<td>{{ $ins->name }}</td>
							<td>{{ $ins->address }}</td>
							<td>{{ $ins->phone_number }}</td>
							<td>
								<div class="ui mini buttons">
									<a href="{{ action('InstituteController@edit', ['id' => $ins->id]) }}" class="ui yellow basic button"><i class="pencil icon"></i></a>
									<a class="ui red basic button btn-delete" data-url="{{ action('InstituteController@destroy', $ins->id) }}" data-title="{{ $ins->name }}"><i class="trash icon"></i></a>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{!! $instansi->render() !!}
		</div>
	</div>
@endsection
