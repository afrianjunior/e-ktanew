@extends('cp.master')

@section('content')

<div class="ui grid">
	<div class="sixteen wide column">
		<h1 class="ui header">Add Institute</h1>

		      <!-- Status Check -->
        @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
        @endif
        <!-- Status Check -->

		{!! Form::open(['route' => 'cp.instansi.store', 'method' => 'post', 'files' => true, 'class' => 'ui form']) !!}

			<div class="field {{ ($errors->first('nama')) ? 'has-error' : '' }}">
				<label for="nama">Nama</label>
				{!! Form::text('nama', null, ['class' => 'form-control']) !!}
				{!! ($errors->first('nama')) ? $errors->first('nama', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="field {{ ($errors->first('no_telp')) ? 'has-error' : '' }}">
				<label for="no_telp">No. Telepon</label>
				{!! Form::text('no_telp', null, ['class' => 'form-control']) !!}
				{!! ($errors->first('no_telp')) ? $errors->first('no_telp', '<span class="help-block">:message</span>') : ''!!}
			</div>

			<div class="field {{ ($errors->first('alamat')) ? 'has-error' : '' }}">
				<label for="alamat">Alamat</label>
				{!! Form::textarea('alamat', null, ['rows' => 3, 'class' => 'form-control']) !!}
				{!! ($errors->first('alamat')) ? $errors->first('alamat', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<hr>

			<div class="field">
				<button type="submit" class="ui teal basic button"><i class="save icon"></i>Save</button>
			</div>

		{!! Form::close() !!}
	</div>
</div>

@endsection
