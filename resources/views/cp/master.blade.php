<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_csrf_token" content="{{ csrf_token() }}">

	<title>eKTA Satpol PP</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('css/semantic.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
	<style media="screen">
		body {
			background-color: #f8fafb;
			color: #444444;
			text-shadow: 0px 1px 0px rgba(255, 255, 255, 1);
		}
		h1, h2, h3, h4, h5 {
			color: #444444;
			text-shadow: 0px 1px 0px rgba(255, 255, 255, 1);
		}
		a {
			color: #17599a;
			text-decoration: none;
		}
		a:hover {
			color: #0c74da;
		}
		.dc.containers {
			margin: 60px 0;
		}
		.dc.title {
			font-size: 43px;
		}
		.dc.main {
			border-radius: 0.9em;
			padding: 30px 25px;
			background-color: white;
			overflow: hidden;
			border: 3px solid #00B5AD!important;
		}
		.dc.mansory  {
			-webkit-column-count: 2;
			   -moz-column-count: 2;
			        column-count: 2;
			-webkit-column-gap: 0;
			   -moz-column-gap: 0;
			        column-gap: 0;
		}
		.dc.link {
			font-size: 1.14em;
		}
		.ui.text.menu {
			text-align: right;
		}
		.ui.vertical.text.menu .item {
			margin: 25px 0px;
			/*font-weight: 300;*/
		}
		.ui.vertical.text.menu .item:hover i.icon {
			color: #F2711C!important;
		}
		.ui.vertical.text.menu .item i.icon {
			margin: 0 0 0 10px;
		}
		.ui.feed > .event > .content .extra.text {
			line-height: 1.7285em;
			word-spacing: .1em;
			margin: .9em 0 0;
			font-size: 1.13em;
			padding-left: 17px;
			border-left: 3px solid rgba(0, 181, 173, 0.54);
		}
		.ui.fluid.card{
			display: inline-block;
		}
	</style>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<!-- <section id="top-navbar">
		@include('components.top-navbar')
	</section> -->

	@if(Session::has('error') || Session::has('success'))
		@include('components.alert')
	@endif

	<section id="main-content">
		<div class="ui container">
			<div class="ui container">
				<div class="dc containers">
					<div class="ui secondary labeled icon menu">
					  <div class="item">
					  	<h1 class="dc title">eKTA Satpol PP</h1>
					  </div>
						<div class="item">
							<a href="{{ route('cp.pegawai.create') }}" class="mini ui teal basic button">
								<i class="plus icon"></i>
								Quick add Employee</a>
						</div>
  					<div class="right menu">
							<div class="item">
								<h3 class="ui teal header">Hello!</h3>
							</div>
    					<div class="item">
								@if(Auth::user())
								<h3 class="ui teal header">{{ Auth::user()->name }}
									<div class="sub header">{{ Auth::user()->email }}</div>
								</h3>
								@endif
    					</div>
							<a href="{{ url('/auth/logout')}}" class="item">
									<i class="log out orange icon"></i>
  						</a>
  					</div>
					</div>
					<div class="dc containers">
						<div class="ui stackable grid">
							<div class="four wide column">
								@include('components.left-navbar')
							</div>
							<div class="twelve wide column">
								<div class="dc main">
									@yield('content')
								</div>
							</div>
						</div>
					</div>
					</div>
			</div>
		</div>
	</section>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="{{ asset('js/select2.min.js') }}"></script>
	<script src="{{ asset('js/jquery-2.1.4.js') }}"></script>
	<script src="{{ asset('js/semantic.min.js') }}"></script>
	<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
	<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
