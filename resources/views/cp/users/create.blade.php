@extends('cp.master')

@section('content')

	<div class="ui grid">
		<div class="sixteen wide column">
			<h1 class="ui header">Menambah Data User</h1>

			{!! Form::open(['route' => 'cp.users.store', 'method' => 'post', 'class' => 'ui form']) !!}

				<div class="field {{ ($errors->first('nama')) ? 'has-error' : '' }}">
					<label for="nama">Nama</label>
					{!! Form::text('nama', null, ['class' => 'form-control'])!!}
					{!! ($errors->first('nama')) ? $errors->first('nama', '<span class="help-block">:message</span>') : '' !!}
				</div>

				<div class="field {{ ($errors->first('email')) ? 'has-error' : '' }}">
					<label for="email">Email</label>
					{!! Form::text('email', null, ['class' => 'form-control'])!!}
					{!! ($errors->first('email')) ? $errors->first('email', '<span class="help-block">:message</span>') : '' !!}
				</div>

				<div class="field {{ ($errors->first('password')) ? 'has-error' : '' }}">
					<label for="password">Password</label>
					{!! Form::password('password', ['class' => 'form-control'])!!}
					{!! ($errors->first('password')) ? $errors->first('password', '<span class="help-block">:message</span>') : '' !!}
				</div>

				<div class="field {{ ($errors->first('konfirmasi_password')) ? 'has-error' : '' }}">
					<label for="konfirmasi_password">Konfirmasi Password</label>
					{!! Form::password('konfirmasi_password', ['class' => 'form-control'])!!}
					{!! ($errors->first('konfirmasi_password')) ? $errors->first('konfirmasi_password', '<span class="help-block">:message</span>') : '' !!}
				</div>

				<div class="field {{ ($errors->first('level')) ? 'has-error' : '' }}">
					<label for="level">Level</label>
					{!! Form::select('level', ['user'=> 'User', 'admin' => 'Admin'], null, ['class' => 'ui dropdown'])!!}
					{!! ($errors->first('level')) ? $errors->first('level', '<span class="help-block">:message</span>') : '' !!}
				</div>

				<div class="field">
					<input type="submit" class="ui orange basic button" value="Simpan">
				</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection
