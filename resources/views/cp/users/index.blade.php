@extends('cp.master')

@section('content')

	<div class="ui grid">
		<div class="sixteen wide column">
			<h1 class="ui header">User <a href="{{ route('cp.users.create') }}" class="ui teal basic right floated button"><i class="plus icon"></i>Add Data</a></h1>
			@if(session('success-create'))
			  	<div class="alert alert-success">
			       	{{ session('success-create') }}
			   	</div>
			@elseif(session('error-create'))
			   	<div class="alert alert-danger">
			       	{{ session('error-create') }}
			   	</div>
			@endif

			@if(session('success-update'))
			   	<div class="alert alert-success">
			       	{{ session('success-update') }}
			   	</div>
			@elseif(session('error-update'))
			   	<div class="alert alert-danger">
			       	{{ session('error-update') }}
			   	</div>
			@endif

			@if(session('success-update'))
			   	<div class="alert alert-success">
			       	{{ session('success-update') }}
			   	</div>
			@elseif(session('error-update'))
			   	<div class="alert alert-danger">
			       	{{ session('error-update') }}
			   	</div>
			@endif

			<table class="ui very basic celled table">
				<thead>
					<tr><th>Nama</th>
						<th>Email</th>
						<th>Level</th>
						<th style="width: 120px">Aksi</th>
					</tr>
				</thead>
				<tbody>

					@foreach($users as $user)

						<tr>
							<td>{{ $user->name }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ $user->level }}</td>
							<td>
								<div class="ui tiny buttons">
									<a href="{{ route('cp.users.edit', $user->id) }}" class="ui yellow basic button"><i class="pencil icon"></i></a>
									<a href="#" class="ui red basic button btn-delete" data-url="{{ action('UserController@destroy', $user->id) }}" data-title="{{ $user->name }}"><i class="trash icon"></i></a>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
				{!! $users->render() !!}
			</table>
		</div>
	</div>
@endsection
