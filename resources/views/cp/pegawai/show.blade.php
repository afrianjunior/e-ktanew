@extends('cp.master')

@section('content')

	<div class="ui grid">
		<div class="sixteen wide column">
			<h1 class="ui header">Detail Employee <a href="{{ route('front.pegawai.show', $pegawai->id_number) }}" class="ui basic olive right floated button"><i class="unhide icon"></i> Lihat Live</a></h1>
			<div class="ui two column grid stackable">
				<div class="column">
						<img src="{{ asset($pegawai->photo1) }}" alt="" class="ui medium rounded centered image">
				</div>
				<div class="column">
						<img src="{{ asset($pegawai->photo2) }}" alt="" class="ui medium rounded centered image">
				</div>
			</div>
			<br>
			<div class="ui container">
					<h3>{{ $pegawai->name }}</h3>
					<table class="ui very basic celled table">
						<tbody style="font-weight: bold;">
							<tr>
								<td>NIP</td>
								<td>{{ $pegawai->id_number }}</td>
							</tr>
							<tr>
								<td>No. HP</td>
								<td>{{ $pegawai->phone_number }}</td>
							</tr>
							<tr>
								<td>alamat</td>
								<td>{{ $pegawai->address }}</td>
							</tr>
							<tr>
								<td>golongan</td>
								<td>{{ $pegawai->rank->rank }}</td>
							</tr>
							<tr>
								<td>Jabatan</td>
								<td>{{ $pegawai->position->name }}</td>
							</tr>
							<tr>
								<td>Instansi</td>
								<td>{{ $pegawai->institute->name }} <br> <i>Alamat: {{ $pegawai->institute->address }}</i></td>
							</tr>
							<tr>
								<td>pendidikan</td>
								<td>{{ $pegawai->academicDegree->name }}</td>
							</tr>
							<tr>
								<td>File</td>
								<td><a href="{{ asset($pegawai->file) }}" target="_blank">{{ asset($pegawai->file) }}</a></i></td>
							</tr>
						</tbody>
					</table>
			</div>
		</div>
	</div>
@stop
