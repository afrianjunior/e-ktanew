@extends('cp.master')

@section('content')

	<div class="ui grid">
		<div class="sixteen wide column">
			<h1 class="ui header">Pegawai <small>data master</small> <a href="{{ route('cp.pegawai.create') }}" class="ui teal basic right floated button"><i class="plus icon"></i>Tambah Data</a></h1>
			<!-- <div class="ui divider"></div> -->
			<form class="ui form">
          <div class="inline fields">
						<div class="field">
							{!! Form::select('f_jabatan', $jabatan, Input::get('f_jabatan'), ['class' => 'ui dropdown selection']) !!}
						</div>
						<div class="field">
							{!! Form::select('f_field', ['nama' => 'Nama', 'nip' => 'NIP'], Input::get('f_field'), ['class' => 'ui dropdown selection']) !!}
						</div>
					<div class="field">
							<input type="text" name="query" placeholder="Masukkan kata kunci" value="{{ Input::get('query') }}">
					</div>
					<input type="submit" class="ui olive button" value="Cari">
        </div>
	    </form>

	        @if (session('error-delete'))
	          <div class="alert alert-danger">
	            {{ session('error-delete') }}
	          </div>
	        @endif

	        @if (session('success-delete'))
	          <div class="alert alert-success">
	            {{ session('success-delete') }}
	          </div>
	        @endif

			<hr>

			@if(!is_null(Input::get('query')))
				<blockquote>
				  <code>Menemukan {{ $pegawai->count() }} data.</code>
				</blockquote>
			@endif





			<div class="ui two column grid">
				@foreach($pegawai as $peg)
				<div class="column">
				<div class="ui fluid card">
    <div class="content">
			<img class="right floated mini ui image" src="{{ asset($peg->photo1) }}">
      <a class="header">{{ $peg->name }}</a>
      <div class="meta">
        <span class="date">{{ $peg->id_number }}</span>
      </div>
			<div class="description">
				Phone Number : {{ $peg->phone_number }} <br>
				{{ $peg->position->name }} at Satpol PP {{ $peg->institute->name }}
			</div>
    </div>
    <div class="extra content">
      <a>
        <i class="map icon"></i>
        {{ $peg->address }}
      </a>
    </div>
		<div class="ui tiny bottom attached buttons">
				<a class="ui teal basic button" href="{{ action('EmployeeController@show', ['id' => $peg->id]) }}"><i class="unhide icon"></i></a>
				<a class="ui green basic button" href="{{ action('EmployeeController@getFile', ['id' => $peg->id]) }}"><i class="download icon"></i></a>
				<a class="ui orange basic button" href="{{ action('EmployeeController@edit', ['id' => $peg->id]) }}"><i class="edit icon"></i></a>
				<a class="ui red basic button btn-delete" href="{{ action('EmployeeController@destroy', $peg->id) }}" ><i class="trash icon"></i></a>
    </div>
  </div>
  </div>
			@endforeach
		</div>

			<!-- <table class="ui basic table">
				<thead>
					<tr>
						<th>NIP</th>
						<th>Nama</th>
						<th>No. HP</th>
						<th>Jabatan</th>
						<th>Instansi</th>
						<th>Alamat</th>
						<th style="width: 100px">Aksi</th>
						<th style="width: 100px">Lainnya</th>
					</tr>
				</thead>
				<tbody>

					@foreach($pegawai as $peg)

						<tr>
							<td>{{ $peg->id_number }}</td>
							<td>{{ $peg->name }}</td>
							<td>{{ $peg->phone_number }}</td>
							<td>{{ $peg->position->name }}</td>
							<td>{{ $peg->institute->name }}</td>
							<td>{{ $peg->address }}</td>
							<td>
								<a href="{{ action('EmployeeController@edit', ['id' => $peg->id]) }}" class="btn btn-warning btn-xs btn-block">Ubah</a>
								<a class="btn btn-danger btn-xs btn-block btn-delete" data-url="{{ action('EmployeeController@destroy', $peg->id) }}" data-title="{{ $peg->name }}">Hapus</a>
							</td>
							<td>
								<a href="{{ action('EmployeeController@show', ['id' => $peg->id]) }}" class="btn btn-primary btn-xs btn-block">Lihat</a>
								<a href="{{ action('EmployeeController@getFile', ['id' => $peg->id]) }}" class="btn btn-default btn-xs btn-block">Download</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table> -->
			{!! $pegawai->render() !!}
		</div>
	</div>
@endsection
