@extends('cp.master')

@section('content')

<div class="ui grid">
	<div class="sixteen wide column">
		<h1 class="ui header">Mengubah Data Pegawai</h1>

		@if(Session::has('error'))
			<div class="alert alert-danger">
				{{ Session::get('error') }}
			</div>
		@endif

		{!! Form::model($pegawai, ['route' => ['cp.pegawai.update', $pegawai->id], 'method' => 'put', 'files' => true, 'class' => 'ui form']) !!}

			<div class="form-group {{ ($errors->first('nip')) ? 'has-error' : '' }}">
				<label for="nip">NIP</label>
				{!! Form::text('nip', $pegawai->id_number, ['class' => 'form-control']) !!}
				{!! ($errors->first('nip')) ? $errors->first('nip', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="form-group {{ ($errors->first('nama')) ? 'has-error' : '' }}">
				<label for="nama">Nama</label>
				{!! Form::text('nama', $pegawai->name, ['class' => 'form-control']) !!}
				{!! ($errors->first('nama')) ? $errors->first('nama', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="field {{ ($errors->first('no_telp')) ? 'has-error' : '' }}">
				<label for="no_telp">No. HP</label>
				{!! Form::text('no_telp', $pegawai->phone_number, ['class' => 'form-control']) !!}
				{!! ($errors->first('no_telp')) ? $errors->first('no_telp', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="field {{ ($errors->first('pendidikan')) ? 'has-error' : '' }}">
				<label for="pendidikan">Pendidikan</label>
				{!! Form::select('pendidikan', $pendidikan, $pegawai->academic_degree_id, ['id' => 'pendidikanPegawai', 'class' => 'ui search dropdown selection']) !!}
				{!! ($errors->first('pendidikan')) ? $errors->first('pendidikan', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="field {{ ($errors->first('instansi')) ? 'has-error' : '' }}">
				<label for="instansi">Instansi</label>
				{!! Form::select('instansi', $instansi, $pegawai->institute_id, ['id' => 'instansiPegawai', 'class' => 'ui search dropdown selection']) !!}
				{!! ($errors->first('instansi')) ? $errors->first('instansi', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="field {{ ($errors->first('jabatan')) ? 'has-error' : '' }}">
				<label for="jabatan">Jabatan</label>
				{!! Form::select('jabatan', $jabatan, $pegawai->position_id, ['id' => 'jabatanPegawai', 'class' => 'ui search dropdown selection']) !!}
				{!! ($errors->first('jabatan')) ? $errors->first('jabatan', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="field {{ ($errors->first('golongan')) ? 'has-error' : '' }}">
				<label for="golongan">Golongan</label>
				{!! Form::select('golongan', $golongan, $pegawai->rank_id, ['id' => 'golonganPegawai', 'class' => 'ui search dropdown selection']) !!}
				{!! ($errors->first('golongan')) ? $errors->first('golongan', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="field {{ ($errors->first('alamat')) ? 'has-error' : '' }}">
				<label for="alamat">Alamat</label>
				{!! Form::textarea('alamat', $pegawai->address, ['rows' => 3, 'class' => 'form-control']) !!}
				{!! ($errors->first('alamat')) ? $errors->first('alamat', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="ui two column grid stackable">
				<div class="column">
					<img src="{{ asset($pegawai->photo1) }}" class="ui medium centered rounded image" alt="">
				</div>
				<div class="column">
					<div class="form-group {{ ($errors->first('foto1')) ? 'has-error' : '' }}">
						<label for="foto1">Foto 1</label>
						<input type="file" name="foto1">
						{!! ($errors->first('foto1')) ? $errors->first('foto1', '<span class="help-block">:message</span>') : '' !!}
						<span class="help-block">* Pilih Untuk mengganti foto 1.</span>
					</div>
				</div>
			</div>

			<hr>

			<div class="ui two column grid stackable">
				<div class="column">
					<img src="{{ asset($pegawai->photo2) }}" class="ui medium centered rounded image" alt="">
				</div>
				<div class="column">
					<div class="form-group {{ ($errors->first('foto2')) ? 'has-error' : '' }}">
						<label for="foto2">Foto 2</label>
						<input type="file" name="foto2">
						{!! ($errors->first('foto2')) ? $errors->first('foto2', '<span class="help-block">:message</span>') : '' !!}
						<span class="help-block">* Pilih Untuk mengganti foto 2.</span>
					</div>
				</div>
			</div>

			<hr>

			<div class="form-group {{ ($errors->first('file')) ? 'has-error' : '' }}">
				<label for="file">File</label>
				<input type="file" name="file">
				{!! ($errors->first('file')) ? $errors->first('file', '<span class="help-block">:message</span>') : '' !!}
				<span class="help-block">* Pilih Untuk mengganti file.</span>
				<br>
				<a href="{{ asset($pegawai->file) }}"  target="_blank" class="ui yellow button">View File</a>
			</div>

			<hr>

			<div class="form-group">
				<button type="submit" class="ui orange basic button"><i class="save icon"></i>Save</button>
			</div>

		{!! Form::close() !!}
	</div>
</div>

@endsection
