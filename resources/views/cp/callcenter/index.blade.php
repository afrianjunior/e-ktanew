@extends('cp.master')

@section('content')

	<div class="ui grid">
		<div class="sixteen wide column">
			<h1 class="ui header">Call Center <small>data master</small></h1>
			<div class="ui grid">
				<div class="ten wide column">

					@if(session('success-create'))
			        	<div class="alert alert-success">
			            	{{ session('success-create') }}
			          	</div>
			        @elseif(session('error-create'))
			        	<div class="alert alert-danger">
			            	{{ session('error-create') }}
			          	</div>
			         @endif

			         @if(session('success-delete'))
			        	<div class="alert alert-success">
			            	{{ session('success-delete') }}
			          	</div>
			        @elseif(session('error-delete'))
			        	<div class="alert alert-danger">
			            	{{ session('error-delete') }}
			          	</div>
			         @endif

					<table class="ui very basic celled table">
						<thead>
							<tr>
								<th>No. Call Center</th>
								<th width="120px">Aksi</th>
							</tr>
						</thead>
						<tbody>

							@foreach($callcenters as $cc)

							<tr>
								<td>{{ $cc->phone_number }}</td>
								<td>
									<div class="ui tiny buttons">
										<a href="{{ route('cp.callcenter.edit', $cc->id) }}" class="ui yellow basic button btn-xs"><i class="pencil icon"></i></a>
										<a class="ui red basic button btn-delete" data-title="{{ $cc->phone_number }}" data-url="{{ action('CallCenterController@destroy', $cc->id) }}"><i class="trash icon"></i></a>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="six wide column">
					<div class="ui piled segments">
						<div class="ui secondary segment">
							Tambah Data
						</div>
						<div class="ui segment">
							{!! Form::open(['route' => 'cp.callcenter.store', 'method' => 'post', 'class' => 'ui form']) !!}
								<div class="field {{ ($errors->first('no_telepon')) ? 'has-error' : '' }}">
									<label for="instansi">Instansi</label>
									{!! Form::select('instansi', $instansi, null, ['id' => 'instansiPegawai', 'class' => 'ui dropdown']) !!}
									{!! ($errors->first('instansi')) ? $errors->first('instansi', '<span class="help-block">:message</span>') : '' !!}
									<label for="no_telepon">No. Telepon</label>
									<input type="text" name="no_telepon" class="form-control">
									{!! ($errors->first('no_telepon')) ? $errors->first('no_telepon', '<span class="help-block">:message</span>') : '' !!}
								</div>
								<div class="field">
									<input type="submit" class="ui teal basic fluid button" value="Save">
								</div>
							{!! Form::close() !!}
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection
