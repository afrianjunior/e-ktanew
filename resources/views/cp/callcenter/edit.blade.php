@extends('cp.master')

@section('content')

	<div class="ui grid">
		<div class="sixteen wide column">
			<h1 class="ui header">Ubah Call Center</h1>
			@if(session('success-update'))
			   	<div class="alert alert-success">
			       	{{ session('success-update') }}
			   	</div>
			@elseif(session('error-update'))
			   	<div class="alert alert-danger">
			       	{{ session('error-update') }}
			   	</div>
			@endif

			{!! Form::model($callcenter, ['route' => ['cp.callcenter.update', $callcenter->id], 'method' => 'put', 'class' => 'ui form']) !!}
				<div class="field">
					<label for="instansi">Instansi</label>
					{!! Form::select('instansi', $instansi, $callcenter->institute_id, ['id' => 'instansiPegawai', 'class' => 'ui dropdown']) !!}
					{!! ($errors->first('instansi')) ? $errors->first('instansi', '<span class="help-block">:message</span>') : '' !!}
				</div>
				<div class="field">
					<label for="no_telepon">No. Telepon</label>
					{!! Form::text('no_telepon', $callcenter->phone_number, ['class' => 'form-control']) !!}
					{!! ($errors->first('no_telepon') ?  $errors->first('no_telepon', '<span class="help-block">:message</span>') : '') !!}
				</div>

				<div class="field">
					<input type="submit" class="ui orange basic button" value="Save">
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
