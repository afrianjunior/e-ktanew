@extends('cp.master')

@section('content')

	<div class="ui grid">
		<div class="sixteen wide column">
			<h1 class="ui header">Position</h1>

			<div class="ui grid">

				<div class="twelve wide column">
					@if(session('success-create'))
			        	<div class="alert alert-success">
			            	{{ session('success-create') }}
			          	</div>
			        @elseif(session('error-create'))
			        	<div class="alert alert-danger">
			            	{{ session('error-create') }}
			          	</div>
			         @endif

			         @if(session('success-delete'))
			        	<div class="alert alert-success">
			            	{{ session('success-delete') }}
			          	</div>
			        @elseif(session('error-delete'))
			        	<div class="alert alert-danger">
			            	{{ session('error-delete') }}
			          	</div>
			         @endif

					<table class="ui very basic celled table">
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama</th>
								<th style="width: 120px;">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							@foreach($jabatan as $jab)

								<tr>
									<td>{{ $no }}</td>
									<td>{{ $jab->name }}</td>
									<td>
										<div class="ui mini buttons">
											<a href="{{ route('cp.jabatan.edit', $jab->id) }}" class="ui yellow basic button"><i class="pencil icon"></i></a>
											<a href="#" class="ui red basic button btn-delete" data-url="{{ action('PositionController@destroy', $jab->id) }}" data-title="{{ $jab->name }}"><i class="trash icon"></i></a>
										</div>
									</td>
								</tr>
								<?php $no++; ?>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="four wide column">
					<div class="ui piled segments">
						<div class="ui secondary segment">
							Add Data
						</div>
						<div class="ui segment">
							<form action="{{ route('cp.jabatan.store') }}" method="post" class="ui form">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="field {{ ($errors->first('nama') ? 'has-error' : '') }}">
									<label for="">Nama</label>
									<input type="text" class="form-control" name="nama" placeholder="Nama Jabatan">
									{{ ($errors->first('nama')) ? $errors->first('nama', '<span class="help-block">:message</span>') : '' }}
								</div>
								<div class="field">
									<input type="submit" class="ui teal basic fluid button" value="Add">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
