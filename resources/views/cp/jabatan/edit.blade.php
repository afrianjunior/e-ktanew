@extends('cp.master')

@section('content')

	<div class="ui grid">
		<div class="sixteen wide column">
			<h1 class="ui header">Edit Data Jabatan</h1>

			@if(session('success-update'))
		      	<div class="alert alert-success">
		           	{{ session('success-update') }}
		       	</div>
		    @elseif(session('error-update'))
		       	<div class="alert alert-danger">
		          	{{ session('error-update') }}
		       	</div>
		    @endif

			{!! Form::model($jabatan, ['route' => ['cp.jabatan.update', $jabatan->id], 'method' => 'put', 'class' => 'ui form']) !!}

				<div class="field">
					<label for="nama">Nama</label>
					{!! Form::text('nama', $jabatan->name, ['class' => 'form-control']) !!}
				</div>

				<div class="field">
					<input type="submit" class="ui orange basic button" value="Save">
				</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection
