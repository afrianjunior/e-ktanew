<!-- Header -->
	<header id="header">
		<h1><a href="/">e-KTA Satpol PP</a></h1>
		<nav id="nav">
			<ul>
				@if(Auth::user())							
				<li><a href="{{ route('cp.dashboard') }}">Dashboard</a></li>
				<li>
					<a href="#" class="icon fa-angle-down">Halo, {{ Auth::user()->nama }}</a>
					<ul>
						<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
					</ul>
				</li>
			
				@else
				<li><a href="{{ url('/auth/login') }}" class="button">Login</a></li>
				@endif
			</ul>
		</nav>
	</header>