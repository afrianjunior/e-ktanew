<!DOCTYPE HTML>
<!--
	Alpha by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>e-KTA Satpol PP</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<style>
			#cta {
			  background: #9b59b6;
			}

			#cta input[type="submit"], #cta input[type="reset"], #cta input[type="button"], #cta .button {
			  color: #9b59b6;
			}

			.box .login {
				width: 320px;
				margin:0 auto;
			}

			div.alert {
				padding: 15px;
				margin-bottom: 20px;
				border: 1px solid transparent;
				border-radius: 4px;
			}

			div.alert-danger {
				color: #a94442;
				background-color: #f2dede;
				border-color: #ebccd1;
			}

			div.alert-danger b{
				color: #A04341;
				font-weight: 600;
				letter-spacing: 1px;
			}

			.has-error {
				/*color:  #f2dede;*/
			}

			.has-error input[type="text"], .has-error input[type="email"], .has-error input[type="password"], .has-error textarea {
				border: 1px solid #a94442;
			}

			.has-error span.help-block {
				color: #a94442;;
			}

		</style>
	</head>
	<body>
		<div id="page-wrapper">

			@include('front.nav')
			@yield('banner')
			@yield('sections')

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>						
					</ul>
					<ul class="copyright">
						<!-- Don't remove  the design's credit Folks! Please appreciate the author. -->
						<li>&copy; e-KTA Satpol PP. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
		<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
		<script src="{{ asset('assets/js/jquery.dropotron.min.js') }}"></script>
		<script src="{{ asset('assets/js/jquery.scrollgress.min.js') }}"></script>
		<script src="{{ asset('assets/js/skel.min.js') }}"></script>
		<script src="{{ asset('assets/js/util.js') }}"></script>
		<!--[if lte IE 8]><script src="{{ asset('assets/js/ie/respond.min.js') }}"></script><![endif]-->
		<script src="{{ asset('assets/js/main.js') }}"></script>

	</body>
</html>