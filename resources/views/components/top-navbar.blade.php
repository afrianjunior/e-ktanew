<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	        </button>
			<a href="#" class="navbar-brand">eKTA</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="{{ url('/') }}">Home</a></li>
				@if(Auth::user())
				<li><a href="{{ route('cp.dashboard') }}">Dashboard</a></li>
				@endif
			</ul>
			<ul class="nav navbar-nav navbar-right">				
				@if(Auth::user())
				<li><a href="{{ action('EmployeeController@index') }}">Pegawai</a></li>
				<li><a href="{{ action('InstituteController@index') }}">Instansi</a></li>
				<li><a href="{{ action('PositionController@index') }}">Jabatan</a></li>
				<li><a href="{{ action('CallCenterController@index') }}">Call Center</a></li>
				<li><a href="{{ action('UserController@index') }}">User</a></li>
				<li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
		            <ul class="dropdown-menu">
		            	<li><a href="{{ route('cp.users.edit', Auth::id()) }}">Edit Profil</a></li>
		            	<li><a href="{{ url('/auth/logout')}}">Logout</a></li>
		        	</ul>
		        </li>
		        @else
		        <li><a href="">Login</a></li>
		        @endif
			</ul>
		</div>
	</div>
</nav>
