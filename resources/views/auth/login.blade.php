@extends('front.layout')

@section('sections')
	<section id="main" class="container">
        <header>
            <h2>Login</h2>
            <p>Silahkan masukkan identitas akun Anda.</p>
        </header>
        <div class="row">
           <div class="12u">
               <section class="box">
                  
                   <div class="login">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                       {!! Form::open(['url' => '/auth/login']) !!}
                            <div class="row uniform 50%">
                                <div class="12u {{ ($errors->first('email')) ? 'has-error' : '' }}">
                                    <label for="">Email</label>
                                    {!! Form::text('email', Input::old('email'), ['class' => 'form-control', 'placeholder' => 'Masukkan email anda']) !!}
                                    {!! ($errors->first('email')) ? $errors->first('email', '<span class="help-block">:message</span>') : '' !!}
                                </div>
                            </div>
                           <div class="row uniform 50%">
                               <div class="12u {{ ($errors->first('password')) ? 'has-error' : '' }}">                                
                                   <label for="">Password</label>
                                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                                    {!! ($errors->first('password')) ? $errors->first('password', '<span class="help-block">:message</span>') : '' !!}                                
                               </div>
                           </div>
                           <div class="row uniform 50%">
                               <div class=" 12u(narrower)">
                                    <input type="checkbox" id="" name="remember">
                                    <label for="remember">Ingat saya</label>
                                </div>
                           </div>
                            <div class="row uniform 50%">
                               <div class="12u">                                
                                  <input type="submit" class="btn btn-primary" value="Log In">
                               </div>
                           </div>
                        {!! Form::close() !!}
                   </div>

               </section>
           </div>
        </div>
    </section>
@stop